<?php
/* ====================
[BEGIN_COT_EXT]
Name=turbo
Description=Yandex турбо страницы
Version=1.0.0
Date=2020-02-07
Author=Cotonti Team & Evgeniy Yalovoy
Copyright=(c)Evgeniy Yalovoy & Cotonti Team 2008-2020 and Above
Auth_guests=R
Lock_guests=A
Auth_members=R
Lock_members=
Recommends_modules=page,forums
Recommends_plugins=comments,attacher
[END_COT_EXT]

[BEGIN_COT_EXT_CONFIG]
turbo_timetolive=02:select:0,10,20,30,40,50,60,120,180,140,200:30:
turbo_maxitems=03:string::40:
turbo_charset=04:string::UTF-8:
turbo_pagemaxsymbols=05:string:::
turbo_postmaxsymbols=06:string:::
[END_COT_EXT_CONFIG]
==================== */

/**
 * turbo setup file
 *
 * @package turbo
 * @copyright (c) y-ea.ru
 */
