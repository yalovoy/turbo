<!-- BEGIN: MAIN -->
<?xml version='1.0' encoding='{TURBO_ENCODING}'?>
<rss xmlns:yandex="http://news.yandex.ru" xmlns:media="http://search.yahoo.com/mTURBO/" xmlns:turbo="http://turbo.yandex.ru" version="2.0">
    <channel>
        <title>{TURBO_TITLE}</title>
        <link>{TURBO_LINK}</link>
        <description>{TURBO_DESCRIPTION}</description>
        <language>{TURBO_LANG}</language>
        <turbo:cms_plugin>62ECB66114C550CC7E0874AB76F3CBB6</turbo:cms_plugin>
        <figure>
            <img src="{PHP.cfg.mainurl}/{PHP.cfg.themes_dir}/{PHP.cfg.defaulttheme}/images/logo.png"></img>
        </figure>
        <!-- BEGIN: ITEM_ROW -->
        <item turbo="true">
            <link>
            <![CDATA[{TURBO_ROW_LINK}]]>
            </link>
            <turbo:source></turbo:source>
            <turbo:topic></turbo:topic>
            <pubDate>{TURBO_ROW_DATE}</pubDate>
            <title>{TURBO_ROW_TITLE}</title>
            <yandex:related></yandex:related>
            <turbo:content>
                <!-- IF {PHP.item.fields.ID|att_count('page',$this,'logo','images')} > 0 -->
                <img src="{PHP.cfg.mainurl}/{PHP.item.fields.ID|att_get('page',$this,'logo')|att_thumb($this,420,280,'crop')}" type="{PHP.item.fields.ID|att_get('page',$this,'logo')|att_thumb($this,420,280,'crop')|att_getMime}" length="{PHP.item.fields.ID|att_get('page',$this,'logo')|att_thumb($this,420,280,'crop')|filesize}"></img>
                <!-- ENDIF -->
                <!-- IF {PHP.item.fields.ID|att_count('market',$this,'','images')} > 0 -->
                <img src="{PHP.cfg.mainurl}/{PHP.item.fields.ID|att_get('market',$this,'')|att_thumb($this,420,280,'crop')}" type="{PHP.item.fields.ID|att_get('market',$this,'')|att_thumb($this,420,280,'crop')|att_getMime}" length="{PHP.item.fields.ID|att_get('market',$this,'')|att_thumb($this,420,280,'crop')|filesize}"></img>
                <!-- ENDIF -->
                <![CDATA[{TURBO_ROW_DESCRIPTION}]]>
                <link>
                <![CDATA[{TURBO_ROW_LINK}]]>
                </link>
            </turbo:content>
        </item>
        <!-- END: ITEM_ROW -->
    </channel>
</rss>
<!-- END: MAIN -->
