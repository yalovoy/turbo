<?php
/**
 * English Language File for the Yandex turbo page Module (rss.en.lang.php)
 *
 * @package Turbo
 */

defined('COT_CODE') or die('Wrong URL.');

/**
 * Module Section
 */

$L['cfg_turbo_timetolive'] = 'Refresh cache every N seconds';
$L['cfg_turbo_timetolive_hint'] = ' ';
$L['cfg_turbo_maxitems'] = 'Max. items in yandex turbo pages feed';
$L['cfg_turbo_maxitems_hint'] = ' ';
$L['cfg_turbo_charset'] = 'RSS charset';
$L['cfg_turbo_charset_hint'] = ' ';
$L['cfg_turbo_pagemaxsymbols'] = 'Pages. Cut element description longer than N symbols';
$L['cfg_turbo_pagemaxsymbols_hint'] = 'Disabled by default';
$L['cfg_turbo_postmaxsymbols'] = 'Posts. Cut element description longer than N symbols';
$L['cfg_turbo_postmaxsymbols_hint'] = 'Disabled by default';

$L['info_desc'] = 'Enables support for yandex turbo pages feeds with website content';

/**
 * Main
 */

$L['turbo_allforums_item_title'] = 'Last forum posts';
$L['turbo_title'] = 'Turbo pages';
$L['turbo_topic_item_desc'] = 'Last topic posts';

/**
 * Errors
 */
$L['turbo_error_private'] = 'This topic is private';
$L['turbo_error_guests'] = 'Not readable for guests';
