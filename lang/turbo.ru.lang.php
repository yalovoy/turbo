<?php
/**
 * English Language File for the Yandex turbo page Module (rss.ru.lang.php)
 *
 * @package Turbo
 */

defined('COT_CODE') or die('Wrong URL.');

/**
 * Module Section
 */

$L['cfg_turbo_timetolive'] = 'Частота обновления кэша';
$L['cfg_turbo_timetolive_hint'] = '(в секундах)';
$L['cfg_turbo_maxitems'] = 'Макс. количество элементов в турбо страницах';
$L['cfg_turbo_maxitems_hint'] = '';
$L['cfg_turbo_charset'] = 'Кодировка турбо страниц';
$L['cfg_turbo_charset_hint'] = 'Набор символов (кодовая страница)';
$L['cfg_turbo_pagemaxsymbols'] = 'Макс. количество символов для страниц';
$L['cfg_turbo_pagemaxsymbols_hint'] = 'По умолчанию отключено';
$L['cfg_turbo_postmaxsymbols'] = 'Макс. количество символов для сообщений форума';
$L['cfg_turbo_postmaxsymbols_hint'] = 'По умолчанию отключено';

$L['info_desc'] = 'Доступ к лентам турбо страниц с контентом вашего сайта';

/**
 * Main
 */

$L['turbo_allforums_item_title'] = 'Последние сообщения на форуме';
$L['turbo_title'] = 'Турбо страницы';
$L['turbo_topic_item_desc'] = 'Последние сообщения в теме';

/**
 * Errors
 */
$L['turbo_error_private'] = 'Это частная тема, доступная только автору и модераторам';
$L['turbo_error_guests'] = 'Тема не доступна для не авторизованного пользователя';
